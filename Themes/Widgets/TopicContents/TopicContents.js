$(function () {
  if (window.c1WidgetTarget == "doAfter") return;
  if (window.c1WidgetTarget == "editor") return;
  $(".c1-widg-TopicContents").each(function () {
    var $this = $(this);
    if ($this.data("toc-tocify")) return;
    $this.tocify((window.c1WidgetOptions || {}).TopicContents);
  });
});


$(function () {
  if (window.c1WidgetTarget == "doAfter") return;

  function getProperty(context, propName) {
    return $(context).prevAll('[data-name="' + propName + '"]').text();
  }

  function getWidgetProperty(context, propName) {
    return $(context).closest('[data-role="c1-widget"]').attr('data-' + propName);
  }

  $(".c1-widg-wijgallery").each(function () {
    var $this = $(this);
    if ($this.data("wijmo-wijgallery")) return;
    if (window.c1WidgetTarget == "preview") {
      var imagesStr = getProperty(this, 'images').replace(/\n|\r/g, ""),
          images = $.parseJSON(imagesStr) || [];
      $this.find('a').each(function (index) {
        if (images[index]) {
          $(this).attr('href', images[index].image);
        }
      });
    }
    $this.parent('[data-role="c1-widget-inner"]').addClass('c1-widg-wijgallery-inner');
    $this.wijgallery({
      showControlsOnHover: true,
      thumbsDisplay: parseInt(getProperty(this, 'thumbsDisplay')),
      thumbsLength: parseInt(getProperty(this, 'thumbHeight')) + 20,
      autoPlay: getProperty(this, 'autoPlay').toLowerCase() === 'true',
      interval: parseInt(getProperty(this, 'delay'))
    })
  });

});

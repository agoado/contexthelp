widgets.widgets["Gallery"].getAuxiliaryFiles = function (props) {
    var coll = [];
    $.each($.parseJSON(props.images), function (index, value) {
        coll.push(value.image);
    });
    return coll;
}

widgets.widgets["Gallery"].beforeAddWidget = function () {
  var 
    files = cs.GetFilesDlg("", "", "").split("|"),
    coll = [];
  $.each(files, function (index, value) {
    coll.push(
      {
        image: value
      });
  });
  if (coll.length > 0) {
    widgets.widgets["Gallery"].properties["images"].defaultValue = JSON.stringify(coll);
  }
  return JSON.stringify(coll);
}

widgets.widgets["Gallery"].onGetWidgetDim = function (dim, props, options) {
  if (options) {
    if (options.targetType === 'manual' || options.targetType === 'epub') {
      return {};
    }
    else {
      var 
        thHeight = parseInt(props.thumbHeight),
        h = dim.height ? parseInt(dim.height.replace('px', '')) - thHeight - 10
          : props.imageList.length && props.imageList[0].image != "" ? cs.GetImageBounds(props.imageList[0].image).split('|')[1]
          : 0,
        w = dim.width ? dim.width
          : props.imageList.length && props.imageList[0].image != "" ? cs.GetImageBounds(props.imageList[0].image).split('|')[0]
          : 0;
      if (options.targetType !== 'nethelp' && options.targetType !== 'editor' && options.targetType !== 'preview') {
        dim.overflowX = 'auto';
        dim.overflowY = 'hidden';
        dim.height = h + 'px';
        props.imgHeight = h - 30 + 'px';
      }
      props.dumbImgHeight = h + 'px';
      props.dumbImgWidth = w;
    }
  }
  return dim;
}

widgets.widgets["Gallery"].onPropsChanged = function (strObject) {
  var obj = $.parseJSON(strObject);
  if (obj.delay < 0) obj.delay = 0;
  if (obj.thumbHeight < 0) obj.thumbHeight = 0;
  if (obj.thumbsDisplay < 0) obj.thumbsDisplay = 0;
  return JSON.stringify(obj);
}

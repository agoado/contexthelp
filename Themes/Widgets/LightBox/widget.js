widgets.widgets["LightBox"].getAuxiliaryFiles = function (props) {
    var coll = [];
    $.each($.parseJSON(props.images), function (index, value) {
        coll.push (value.image);
        coll.push(value.thumbnail);
        coll.push(value.url);
    });
    return coll;
}

widgets.widgets["LightBox"].beforeAddWidget = function () {
  var 
    files = cs.GetFilesDlg("", "", "").split("|"),
    coll = [];
  $.each(files, function (index, value) {
    coll.push(
      {
        image: value,
        thumbnail: value,
        type: "image"
      });
  });
  if (coll.length > 0) {
    widgets.widgets["LightBox"].properties["images"].defaultValue = JSON.stringify(coll);
  }
  return JSON.stringify(coll);
}

widgets.widgets["LightBox"].onPropsChanged = function (strObject) {
  var obj = $.parseJSON(strObject);
  if (obj.delay && obj.delay < 0) obj.delay = 0;
  if (obj.thumbHeight && obj.thumbHeight < 0) obj.thumbHeight = 0;
  if (obj.fullHeight && obj.fullHeight < 0) obj.fullHeight = 0;
  if (obj.fullWidth && obj.fullWidth < 0) obj.fullWidth = 0;
  if (obj.type) {
    obj.disableProperties = "";
    if (obj.type === "image") {
      obj.disableProperties = "url";
    }
    else if (obj.type === "external content") {
      obj.disableProperties = "image";
    }
  }
  return JSON.stringify(obj);
}

widgets.widgets["LightBox"].onGetWidgetDim = function (dim, props, options) {
  if (dim.height || props.thumbHeight) {
    props.thumbnailHeight = 'height:' + (props.thumbHeight ? props.thumbHeight : parseInt(dim.height) - 7) + 'px';
  }
  else {
    props.thumbnailHeight = 'height:80px';
  }
  return dim;
}

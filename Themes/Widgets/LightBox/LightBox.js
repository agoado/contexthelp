function dumbPopup(el) {
  return d2hpopup(el.getAttribute("href"));
}

$(function () {
    if (window.c1WidgetTarget == "doAfter") return;

    function getProperty(context, propName) {
        return $(context).prevAll('[data-name="' + propName + '"]').text();
    }
    $(".c1-widg-lightbox-dumb").each(function () {
        var $this = $(this),
        imagesStr = getProperty(this, 'images').replace(/\n|\r/g, ""),
        images = $.parseJSON(imagesStr) || [];
        $this.find('a').each(function (index) {
            if (images[index] && images[index].type === 'image') {
                $(this).click(function () { dumbPopup(this); return false; });
            }
        });
    });
    $(".c1-widg-lightbox").each(function () {
        var $this = $(this);
        if ($this.data("wijmo-wijlightbox")) return;
        var imagesStr = getProperty(this, 'images').replace(/\n|\r/g, ""),
        images = $.parseJSON(imagesStr) || [];
        if (window.c1WidgetTarget == "preview") {
            $this.find('a').each(function (index) {
                if (images[index] && images[index].type === 'image') {
                    $(this).attr('href', images[index].image);
                }
                else {
                    $(this).attr('href', images[index].url);
                }
            });
        }
        var options = {
            modal: getProperty(this, 'modal').toLowerCase() === 'true',
            autoPlay: getProperty(this, 'autoPlay').toLowerCase() === 'true',
            delay: parseInt(getProperty(this, 'delay'))
        },
      height = getProperty(this, 'fullHeight'),
      width = getProperty(this, 'fullWidth');
        if (height)
            options.height = height;
        if (width)
            options.width = width;
        $this.wijlightbox(options);
    })

});

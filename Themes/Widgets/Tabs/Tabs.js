$(function () {
  if (window.c1WidgetTarget == "doAfter" || window.c1WidgetTarget == "editor") return;

  function getProperty(context, propName) {
    return $(context).prevAll('[data-name="' + propName + '"]').text();
  }

  $(".c1-widg-tabs").each(function () {
    var $this = $(this);

    if ($this.data("wijmo-wijtabs")) return;

    $this.find('li a[data-ref]').each(function() {
      $(this).attr('href', $(this).attr('data-ref'));
    });
    $this.wijtabs($.extend({
      alignment: getProperty(this, "alignment")
    }, (window.c1WidgetOptions || {}).Tabs
    ));

  });

});

widgets.widgets["Tabs"].beforeAddWidget = function () {
  var coll = JSON.stringify([
    { id: generateGuid(), title: "Tab 1" },
    { id: generateGuid(), title: "Tab 2" }
  ]);
  widgets.widgets["Tabs"].properties["tabs"].defaultValue = coll;
  return coll;
};

widgets.widgets["Tabs"].beforeSetProperties = function (propNames, propValues) {
  if (!propNames || !propValues) return;
  var i, j, tabs, tab;
  propNames = propNames.split("|");
  propValues = propValues.split("|");
  for (i = 0; i < propNames.length; i++) {
    if (propNames[i] != "tabs") continue;
    tabs = propValues[i];
    tabs = JSON.parse(tabs);
    for (j = 0; j < tabs.length; j++) {
      tab = tabs[j];
      if (!tab.id) {
        tab.id = generateGuid();
      }
    }
    propValues[i] = JSON.stringify(tabs);
    return propValues.join("|");
  }
  return "";
};

function generateGuid() {
  function _p8(s) {
    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
    return s ? p.substr(0, 4) + p.substr(4, 4) : p;
  }
  return _p8() + _p8(true) + _p8(true) + _p8();
}


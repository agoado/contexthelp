widgets.widgets["Carousel"].getAuxiliaryFiles = function (props) {
    var coll = [];
    $.each($.parseJSON(props.images), function (index, value) {
        coll.push(value.image);
    });
    return coll;
}

widgets.widgets["Carousel"].beforeAddWidget = function () {
  var 
    files = cs.GetFilesDlg("", "", "").split("|"),
    coll = [];
  $.each(files, function (index, value) {
    coll.push(
      {
        image: value
      });
  });
  if (coll.length > 0) {
    widgets.widgets["Carousel"].properties["images"].defaultValue = JSON.stringify(coll);
  }
  return JSON.stringify(coll);
}

widgets.widgets["Carousel"].onGetWidgetDim = function (dim, props, options) {
  if (options) {
    if (options.targetType === 'manual' || options.targetType === 'epub') {
      return {};
    }
    else {
      var h = parseInt(dim.height.replace('px', ''));
      if (options.targetType === 'javahelp' || options.targetType === 'htmlhelp' || options.targetType === 'mshelpviewer' || options.targetType === 'mshelp') {
        dim.overflowX = 'auto';
        dim.overflowY = 'hidden';
        props.imgHeight = h - 30 + 'px';
        if (options.targetType === 'javahelp') {
          dim.height = props.imgHeight;
        }
      }
      else if (options.targetType === 'editor') {
        props.dumbHeight = h - 2 < 0 ? 0 : h - 2 + 'px';
        props.dumbImgHeight = h - 43 < 0 ? 0 : h - 43 + 'px';
        props.dumbImgWidth = (parseInt(dim.width) - 1) + 'px';
      }
    }
  }
  return dim;
}

widgets.widgets["Carousel"].onPropsChanged = function (strObject) {
  var obj = $.parseJSON(strObject);
  if (obj.delay < 0) obj.delay = 0;
  return JSON.stringify(obj);
}

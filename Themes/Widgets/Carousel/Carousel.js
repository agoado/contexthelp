$(function () {
  if (window.c1WidgetTarget == "doAfter") return;

  function getProperty(context, propName) {
    return $(context).prevAll('[data-name="' + propName + '"]').text();
  }

  $(".c1-widg-wijcarousel").each(function () {
    var $this = $(this);
    if ($this.data("wijmo-wijcarousel")) return;
    $this.parent('[data-role="c1-widget-inner"]').addClass('c1-widg-wijcarousel-inner');
    $this.parents('[data-role="c1-widget"]').css('margin-bottom', '18px');
    var pos = getProperty(this, 'pagerPosition'),
      pagerPos = {
        my: pos + ' top',
        at: pos + ' bottom'
      };
    $this.wijcarousel({
      display: 1,
      showTimer: true,
      showPager: true,
      loop: true,
      pagerPosition: pagerPos,
      pagerType: "dots",
      auto: getProperty(this, 'autoPlay').toLowerCase() === 'true',
      interval: parseInt(getProperty(this, 'delay'))
    });
  });
});

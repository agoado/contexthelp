widgets.widgets["Note"].getAuxiliaryFiles = function (props) {
    return [props.customImage];
}

widgets.widgets["Note"].onGetWidgetDim = function(dim, props, options) {
  if (options && options.targetType === 'epub' && dim) {
    dim.height = '';
  }
  if (options && options.targetType === 'nethelp' && dim && !dim.width) {
    dim.width = '100%';
  }
  if (dim && dim.height) {
    props.iheight = ' height: ' + (parseInt(dim.height) - 2) + 'px'; 
  }
  return dim;
}

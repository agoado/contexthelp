(function () {
  if (!window.$ || $(".prettyprintProcessed").length > 0) return;
  $(function () {
    switch (window.c1WidgetTarget) {
      case "editor":
      case "manual":
      case "winforms":
      case "epub":
      case "doAfter":
        return;
    }
    initializeCopyCodeLinks()
    if (window.c1WidgetTarget == "preview") {
      prettyPrint();
    }

    function getText() {
      var txt = $(this).parents('[data-role="c1-widget"]').find(".prettyprint");
      return txt.length ? txt[0].innerText : "";
    }

    function showCopyCode() {
      $('div.CopyCodeWrapper').show();
      $('.prettyprint').addClass('prettyprintProcessed');
    }

    // Wire up any span.CopyCode elements to copy to clipboard
    function initializeCopyCodeLinks() {
      // Wire up the copy code functionality
      if (location.protocol == 'mk:' || window.clipboardData && window.clipboardData.setData) {
        showCopyCode();
        // In CHM or IE use the inbuilt IE clipboard support
        $('span.CopyCode').click(function () {
          var textValue = getText.call(this);
          window.clipboardData.setData('Text', textValue);
          alert("Copied text to clipboard:\n\n " + textValue);
        });
      } else if (location.protocol == "file:") {
        /*$('span.CopyCode').click(function () {
        // Cannot copy to clipboard from local content in browsers other than IE
        alert("Cannot copy to the clipboard as browser security restrictions prevent copying to the clipboard from local Html content");
        });*/
      } else {
        showCopyCode();
        // Use zero clipboard for other scenarios
        $('span.CopyCode').zclip({
          path: 'Images/ZeroClipboard.swf',
          copy: getText,
          setHandCursor: true
        });
      }
    };
  });
})();

widgets.widgets["CodeHighlighter"].onPropsChanged = function (strObject) {
  var obj = $.parseJSON(strObject);
  if (obj.linenums < 0) obj.linenums = 0;
  return JSON.stringify(obj);
}

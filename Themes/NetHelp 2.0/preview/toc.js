[
    {
        "url": "Documents/First_Topic.htm",
        "title": "First Topic",
        "tooltip": "First Topic"
    },
    {
        "url": "Documents/Second_Topic.htm",
        "title": "Second Topic",
        "tooltip": "Second Topic",
        "items": [
            {
                "url": "Documents/Subtopic_1.htm",
                "title": "Subtopic 1",
                "tooltip": "Subtopic 1"
            },
            {
                "url": "Documents/Subtopic_2.htm",
                "title": "Subtopic 2",
                "tooltip": "Subtopic 2",
                "items": [
                    {
                        "url": "Documents/ThirdLevelTopic.htm",
                        "title": "Third Level Topic",
                        "tooltip": "Third Level Topic"
                    },
                    {
                        "url": "Documents/thirdleveltopic2.htm",
                        "title": "Third Level Topic 2",
                        "tooltip": "Third Level Topic 2"
                    },
                    {
                        "url": "Documents/tablesample.htm",
                        "title": "Table Sample",
                        "tooltip": "Table Sample"
                    }
                ]
            }
        ]
    },
    {
        "url": "Documents/Third_Topic.htm",
        "title": "Third Topic",
        "tooltip": "Third Topic"
    },
    {
        "url": "Documents/sampletopic.htm",
        "title": "Sample Topic",
        "tooltip": "Sample Topic",
        "items": [
            {
                "url": "Documents/subtopic.htm",
                "title": "Subtopic",
                "tooltip": "Subtopic",
                "items": [
                    {
                        "url": "Documents/thirdleveltopic1.htm",
                        "title": "Third Level Topic",
                        "tooltip": "Third Level Topic"
                    }
                ]
            }
        ]
    },
    {
        "url": "Documents/glossary.htm",
        "glossary": "1",
        "title": "Glossary",
        "tooltip": "Glossary"
    }
]
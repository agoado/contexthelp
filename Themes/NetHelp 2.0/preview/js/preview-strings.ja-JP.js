/* 
* NetHelp Theme Wizard Japanese string resources
*/
(function ($) {
  $["ja-JP"] = {
      previewStrings: function () {
        return {
          loading: "Loading...",
          loadingImage: "...",
          loadingImages: "Loading {0} of {1}",
          steps: {
            chooseLayout: {
              title: "Choose Basic Layout",
              desc: "NetHelp's Responsive layout will work well on any device, but many prefer the traditional look. All layouts are full-featured."
            },
            chooseStylesheet: {
              title: "Choose Style",
              desc: "NetHelp's layout ships with several style sheets. Note that you can edit these in the theme designer or you can roll your own in that <a href='http://nest.doctohelp.com/grapecity/d2hdocs/#!Documents/Creating_and_Adding_New_Style_Sheets.htm' target='_blank'>jQuery Theme Roller.<a/>"
            },
            chooseTheme: {
              title: "Choose Theme",
              desc: "You can create a new theme or choose an existing theme."
            },
            chooseLanguage: {
              title: "Choose Language",
              desc: "We can automatically localize the NetHelp interface for you. If you don't see your language, you can localize the strings yourself in the Theme Designer dialog."
            },
            themeBranding: {
              title: "Basic Branding",
              desc: "NetHelp allows you to brand your output with your logo, title, and favicon."
            },
            targetBranding: {
              title: "Finishing Touches",
              desc: "Below are the most common options people set for NetHelp target."
            },
            themeNaming: {
              title: "Confirmation",
              desc: "You have just configured a theme."
            }
          },
          layouts: {
            Accordion: "Traditional tri-pane layout with accordion panes for contents, search, and index.",
            Tabs: "Traditional tri-pane layout with tabs for contents, search, and index.",
            Responsive: "Modern-looking layout that automatically adjusts to desktop, tablet, and phone sizes.",
            Unknown: "A custom layout."
          },
          themeBranding: {
            readonlyTheme: "You have chosen a predefined theme. It cannot be modified.",
            brandingWarning: "<b>Note:</b> Changes made on this page will affect all your targets that use the selected theme.",
            favIconTitle: "FavIcon",
            favIconDesc: "FavIcons appear in browser tabs, address bars, and in favorites lists. They are a great way to make your site stand out. FavIcons require .ico files.",
            chooseFavIcon: "Choose your icon",
            removeFavIcon: "Remove icon",
            favIconExample: "FavIcon example",
            logoTitle: "Logo",
            logoDesc: "Your logo will appear in the upper-left corner of the header in traditional layouts and at the top of the navigation pane in responsive layouts.",
            chooseLogo: "Choose your logo",
            removeLogo: "Remove logo",
            themeHeader: "Header area (Title bar)",
            showTitle: "Show the Title of your NetHelp output in the top title bar",
            headerHeight: "Header area height",
            textArea: "Text area",
            fitTextWidth: "Fit text to window width",
            maxTextWidth: "Maximum text width"
          },
          targetBranding: {
            targetHeader: "Header Area (Title bar)",
            targetTitleLabel: "Title",
            accessibility: "Accessibility",
            section508Label: "Create Section 508 compliant output",
            languageTitle: "Language",
            languageDesc: "We can automatically localize the NetHelp interface for you.",
            languageNote: "If you don't see your language, you can localize the strings yourself in the Theme Designer dialog."
          },
          themeNaming: {
            themeNameDesc: "In order to save it, we need a name. This theme will be available in the Themes menu from now on.",
            themeNameLabel: "Theme name"
          }
        };
      }
    }
  }
)(jQuery);
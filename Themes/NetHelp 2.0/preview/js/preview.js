/* 
* NetHelp Theme Wizard
*/
(function ($, undefined) {

  $(document).ready(function () {
    wizard.init();
  });

  // wizard object
  var wizard = {
    // properties
    step: "", // the current wizard step name
    strings: {}, // wizard localized strings
    settings: {}, // external theme settings
    formSettings: {}, // user settings for the wizard appearance 
    timeOuts: {}, // timeouts to update wizard previews
    intervals: {}, // intervals to check widget values 
    dropdownOpened: false, // dropdowns current state
    defaultMaxTextWidth: 300, // default topic text maximum width

    // Initialzes wizard
    init: function () {
      /* cache elements */
      wizard.carousel = $("#carousel");
      wizard.imageDecs = $("#imageDecs");
      wizard.imageSelector = $("#imageSelector");
      wizard.languageSelector = $("#languageSelector");
      wizard.headerHeight = $("#headerHeight");
      wizard.maxTextWidth = $("#maxTextWidth");
      wizard.targetTitle = $("#targetTitle");
      wizard.themeName = $("#themeName");
      wizard.progress = $("[data-c1role='progress']");
      wizard.progressLabel = $("[data-c1role='progressText']");
      wizard.previewImages = $("[data-c1role='previewImage']");

      /* localize */
      if (wizard.hasExternal("getLanguage")) {
        // get the application culture 
        var lang = window.external.getLanguage();
      }
      wizard.strings = $[lang] ? $[lang].previewStrings()
        : previewStrings(); // deafult string resources

      /* apply appearance from the user settings */
      if (wizard.hasExternal("getFormSettings")) {
        // get user settings for appearance
        wizard.formSettings = JSON.parse(window.external.getFormSettings());
      }
      wizard.formSettings.brandingSplitter = wizard.formSettings.brandingSplitter || 485;
      wizard.formSettings.targetSplitter = wizard.formSettings.targetSplitter || 485;

      /* apply string resources */
      $("#favIconTitle").text(wizard.strings.themeBranding.favIconTitle);
      $("#favIconDesc").text(wizard.strings.themeBranding.favIconDesc);
      $("#chooseFavIcon").button({ label: wizard.strings.themeBranding.chooseFavIcon });
      $("#favIconExample").attr("title", wizard.strings.themeBranding.favIconExample);
      $("#favIcon").attr("title", wizard.strings.themeBranding.favIconTitle);
      $("#logoTitle").text(wizard.strings.themeBranding.logoTitle);
      $("#logoDesc").text(wizard.strings.themeBranding.logoDesc);
      $("#chooseLogo").button({ label: wizard.strings.themeBranding.chooseLogo });
      $("#logo").attr("title", wizard.strings.themeBranding.logoTitle);
      $("#themeHeader").text(wizard.strings.themeBranding.themeHeader);
      $("#showTitleLabel").text(wizard.strings.themeBranding.showTitle);
      $("#headerHeightLabel").text(wizard.strings.themeBranding.headerHeight);
      $("#topicArea").text(wizard.strings.themeBranding.textArea);
      $("#fitTextWidthLabel").text(wizard.strings.themeBranding.fitTextWidth);
      $("#maxTextWidthLabel").text(wizard.strings.themeBranding.maxTextWidth);
      $("#readonlyTheme").text(wizard.strings.themeBranding.readonlyTheme);
      $("#brandingWarning").html(wizard.strings.themeBranding.brandingWarning);
      $("#targetHeader").text(wizard.strings.targetBranding.targetHeader);
      $("#targetTitleLabel").text(wizard.strings.targetBranding.targetTitleLabel);
      $("#accessibility").text(wizard.strings.targetBranding.accessibility);
      $("#section508Label").text(wizard.strings.targetBranding.section508Label);
      $("#languageTitle").text(wizard.strings.targetBranding.languageTitle);
      $("#languageDesc").text(wizard.strings.targetBranding.languageDesc);
      $("#languageNote").text(wizard.strings.targetBranding.languageNote);
      $("#themeNameDesc").text(wizard.strings.themeNaming.themeNameDesc);
      $("#themeNameLabel").text(wizard.strings.themeNaming.themeNameLabel);

      /* create widgets */
      wizard.themeName.wijinputmask({ hideEnter: true });
      wizard.targetTitle.wijinputmask({ hideEnter: true /* need to be true, otherwise it performs click on other buttons */ });
      $(":checkbox").wijcheckbox();
      $("#themeBrandingSpliiter").wijsplitter({
        orientation: "vertical",
        showExpander: false,
        splitterDistance: wizard.formSettings.brandingSplitter,
        fullSplit: true, // need to be true, otherwise wizard layout is damaged
        sized: function () {
          // save splitter position in the user settings
          wizard.formSettings.brandingSplitter = $(this).wijsplitter("option", "splitterDistance");
          if (wizard.hasExternal("setFormSettings")) {
            window.external.setFormSettings(JSON.stringify(wizard.formSettings));
          }
        }
      });
      $("#targetBrandingSplitter").wijsplitter({
        orientation: "vertical",
        showExpander: false,
        splitterDistance: wizard.formSettings.targetSplitter,
        fullSplit: true, // need to be true, otherwise wizard layout is damaged
        sized: function () {
          // adjust size of controls to the new panel size 
          wizard.adjustCombobox(wizard.languageSelector, "#targetBrandingProperties");
          // save splitter position in the user settings
          wizard.formSettings.targetSplitter = $(this).wijsplitter("option", "splitterDistance");
          if (wizard.hasExternal("setFormSettings")) {
            window.external.setFormSettings(JSON.stringify(wizard.formSettings));
          }
        }
      });
      wizard.imageSelector.wijcombobox({
        isEditable: false,
        showingAnimation: null,
        text: wizard.strings.loadingImage,
        select: function (e, data) {
          // scroll carousel to the selected in the cobobox screenshot
          wizard.carousel.find("img").each(function () {
            var item = $(this);
            if (item.data("value") == data.value) {
              item.trigger("click", { force: true /* C1 addition for the widget: "force" parameter allows to scroll the image to the carusel center even it is hidden right now */ });
              return false;
            }
          });
        },
        open: function () {
          // prevents to go to the next wizard step by pressing Enter key while dropdown is open
          wizard.dropdownOpened = true;
        },
        close: function () {
          wizard.dropdownOpened = false;
        }
      }).width('300px').parents('.wijmo-wijcombobox').width('300px');
      wizard.languageSelector.wijcombobox({
        isEditable: false,
        showingAnimation: null,
        select: function (e, data) {
          // updates the theme preview screenshot
          wizard.previewImages.fadeOut();
          window.external.updateLanguage(data.value);
        },
        open: function () {
          // prevents to go to the next wizard step by pressing Enter key while dropdown is open
          wizard.dropdownOpened = true;
        },
        close: function () {
          wizard.dropdownOpened = false;
        }
      });
      wizard.headerHeight.wijinputnumber({
        decimalPlaces: 0,
        hideEnter: true, // need to be true, otherwise it performs click on other buttons
        maxValue: 1000000,
        minValue: 0, 
        // updates the theme preview screenshot with timeout
        valueChanged: function (e, data) {
          // applay value to the theme settings
          var applyValue = function (refreshPreview) {
            if (wizard.hasExternal("updateThemeBranding")) {
              // don't do it if we are already moved on another wizard step
              if (!wizard.headerHeight.is(":visible")) return;
              refreshPreview && wizard.previewImages.fadeOut();
              wizard.settings.pageHeader.height = data.value;
              var setter = { pageHeader: { height: data.value } };
              window.external.updateThemeBranding(JSON.stringify(setter), refreshPreview);
            }
          };
          wizard.updateNumericValue(wizard.headerHeight, data.value, "headerHeight", applyValue);
        }
      });
      wizard.maxTextWidth.wijinputnumber({
        decimalPlaces: 0,
        hideEnter: true, // need to be true, otherwise it performs click on other buttons
        maxValue: 1000000,
        minValue: 1, 
        // updates the theme preview screenshot with timeout
        valueChanged: function (e, data) {
          // applay value to the theme settings
          var applyValue = function (refreshPreview) {
            // cache the new value for further use
            wizard.defaultMaxTextWidth = data.value;
            if (wizard.hasExternal("updateThemeBranding")) {
              // don't do it if we are already moved on another wizard step
              if (!wizard.maxTextWidth.is(":visible")) return;
              refreshPreview && wizard.previewImages.fadeOut();
              wizard.settings.topic.textWidth = data.value;
              var setter = { topic: { textWidth: data.value } };
              window.external.updateThemeBranding(JSON.stringify(setter), refreshPreview);
            }
          };
          wizard.updateNumericValue(wizard.maxTextWidth, data.value, "maxTextWidth", applyValue);
        }
      });

      /* attach event handlers */
      // scroll theme preview screenshots by mouse wheel 
      wizard.carousel.on("mousewheel", function (e, a, b, c) {
        a < 0 ? wizard.carouselWidget.next() : wizard.carouselWidget.prev();
      }).click(function (e) {
      	var $target = $(e.target);
      	if ($target.hasClass('right')) {
      		wizard.carouselWidget.next();
      	} else if ($target.hasClass('left')) {
      		wizard.carouselWidget.prev();
      	}
      });
      // set the "showText" value to the the theme settings and refresh the selected theme preview
      $("#showTitle").click(function (e, data) {
        var that = $(this);
        var value = that.wijcheckbox("option", "checked");
        // applay value to the theme settings
        var applyValue = function (refreshPreview) {
          if (wizard.hasExternal("updateThemeBranding")) {
            // don't do it if we are already moved on another wizard step
            if (!that.is(":visible")) return;
            refreshPreview && wizard.previewImages.fadeOut();
            wizard.settings.pageHeader.showText = value;
            var setter = { pageHeader: { showText: value } };
            window.external.updateThemeBranding(JSON.stringify(setter), refreshPreview);
          }
        };
        wizard.updateBoolValue(that, "showTitle", applyValue);
      });
      // set the "textWidth" value to 0 (if checked) for the the theme settings and refresh the selected theme preview
      $("#fitTextWidth").click(function (e, data) {
        var that = $(this);
        var checked = that.wijcheckbox("option", "checked");
        var value = checked ? 0 : wizard.defaultMaxTextWidth;
        // applay value to the theme settings
        var applyValue = function (refreshPreview) {
          // disable max topic text width control if text fits to the window
          wizard.maxTextWidth.wijinputnumber("option", "disabled", checked);
          if (wizard.hasExternal("updateThemeBranding")) {
            // don't do it if we are already moved on another wizard step
            if (!that.is(":visible")) return;
            refreshPreview && wizard.previewImages.fadeOut();
            wizard.settings.topic.textWidth = value;
            var setter = { topic: { textWidth: value } };
            window.external.updateThemeBranding(JSON.stringify(setter), refreshPreview);
          }
        };
        wizard.updateBoolValue(that, "fitTextWidth", applyValue);
      });
      // set the "accessibilityMode" value to the the theme settings and refresh the selected theme preview
      $("#section508").click(function (e, data) {
        var that = $(this);
        var value = $(this).wijcheckbox("option", "checked") ? "Section 508" : "";
        // applay value to the theme settings
        var applyValue = function (refreshPreview) {
          if (wizard.hasExternal("updateTargetBranding")) {
            // don't do it if we are already moved on another wizard step
            if (!that.is(":visible")) return;
            refreshPreview && wizard.previewImages.fadeOut();
            wizard.settings.general.accessibilityMode = value;
            var setter = { general: { accessibilityMode: value } };
            window.external.updateTargetBranding(JSON.stringify(setter), refreshPreview);
          }
        };
        wizard.updateBoolValue(that, "section508", applyValue);
      });
      // select a new icon for the "favicon" theme property and refresh the selected theme preview
      $("#chooseFavIcon").click(function () {
        if (wizard.hasExternal("chooseFavIcon")) {
          window.external.chooseFavIcon();
          document.body.focus();
        }
      });
      // select a new image for the "logoImage" theme property and refresh the selected theme preview
      $("#chooseLogo").click(function () {
        if (wizard.hasExternal("chooseLogo")) {
          window.external.chooseLogo();
          document.body.focus();
        }
      });
      // removes the icon for the "favicon" theme property and refresh the selected theme preview
      $("#removeFavIcon").click(function () {
        if (wizard.hasExternal("removeFavIcon")) {
          window.external.removeFavIcon();
        }
      }).attr("title", wizard.strings.themeBranding.removeFavIcon)
        .button({ icons: { secondary: "ui-icon-close" } });
      // removes the image for the "logoImage" theme property and refresh the selected theme preview
      $("#removeLogo").click(function () {
        if (wizard.hasExternal("removeLogo")) {
          window.external.removeLogo();
        }
      }).attr("title", wizard.strings.themeBranding.removeLogo)
        .button({ icons: { secondary: "ui-icon-close" } });
      // put buttons like "choose" and "remove" in a set
      $("[data-c1role='buttonset']").buttonset();
      // workaround for wijinputnumber widgets
      // clears the value when cross sign is clicked
      $("[data-c1role='number']").mouseup(function () {
        var input = $(this);
        if (!input.val()) {
          return;
        }
        setTimeout(function () {
          if (!input.val()) {
            input.wijinputnumber("setValue", 0, true);
          }
        }, 1);
      });
      // workaround for wijinputnumber widgets
      // prevent to assign a negative value
      $("[data-c1role='number']").keydown(function (e) {
        if (e.which == 109 || e.which == 189) {
          e.preventDefault();
        }
      });
      // adjust widget appearence wehn window resize
      $(window).resize(function () {
        // adjust height of all splitters
        $(".splitter").each(function (_, elem) {
          if ($(elem).is(":visible")) {
            $(elem).height($(window).innerHeight() - $(elem).offset().top - 10).wijsplitter("refresh");
          }
        });
        // adjust the final theme preview screenshot
        wizard.adjustPreviewImage("#themeNaming");
        // center carousel
        if (wizard.carouselWidget !== undefined) {
          wizard.carouselWidget.redraw();
        }
      });

      /* create global functions */
      // execute wizard functions from external code
      window.previewExec = function (args) {
        var data = JSON.parse(args);
        return wizard[data.command](data.args);
      };
      // gets the external NetHelp settings
      window.getNethelpExternalSettings = function () {
        if (wizard.hasExternal("onGetNethelpExternalSettings")) {
          return window.external.onGetNethelpExternalSettings();
        }
      };
      // extends the target json object with source json object 
      // and removes in the result inherited properties from the base json object
      window.extendJson = function (target, source, base) {
        var res = $.extend(true, JSON.parse(target), JSON.parse(source));
        if (base) {
          // remove inherited properties
          function removeInherited(x, y) {
            if ($.isPlainObject(x) && $.isPlainObject(y)) {
              for (var name in x) {
                if (removeInherited(x[name], y[name])) {
                  delete x[name];
                }
              }
              return $.isEmptyObject(x);
            } else if ($.isArray(x)) {
              for (i = x.length; i >= 0; i--) {
                if (!x[i]) {
                  x.splice(i, 1);
                }
              }
              return x.length == 0;
            } else {
              return x == y;
            }
          }
          removeInherited(res, JSON.parse(base));
        }
        return JSON.stringify(res);
      };
      // notifies external code the wizard is ready to start
      if (wizard.hasExternal("documentReady")) {
        window.external.documentReady();
      }
    },

    /* called from external code */

    // initializes a wizard step page
    // called from external code
    startStep: function (data) {
      var name = data[0];
      wizard.step = name;
      // stop listening user activity
      clearInterval(wizard.intervals.themeName);
      clearInterval(wizard.intervals.targetTitle);
      // close all dropdowns
      wizard.closeDropdown();
      // hide splitters to smooth appear
      $(".splitter").each(function (_, elem) {
        $(elem).hide();
      });
      // disable ability to change values by the user 
      wizard.imageSelector.wijcombobox("option", "disabled", true);
      $(document.body).css("cursor", "wait");
      // make the appropriate wizard step page visible
      $("#previewSelector").toggle($.inArray(name, [
        "chooseLayout",
        "chooseStylesheet",
        "chooseTheme"]) >= 0);
      $("#themeBranding").toggle(name == "themeBranding");
      $("#targetBranding").toggle(name == "targetBranding");
      $("#themeNaming").toggle(name == "themeNaming");
      // initialize the appropriate wizard step page
      if (name == "themeBranding") {
        wizard.themeBranding();
      } else if (name == "targetBranding") {
        wizard.targetBranding();
      } else if (name == "themeNaming") {
        wizard.themeNaming();
      }
      // reset UI
      wizard.carousel.empty().hide();
      wizard.imageDecs.text("");
      wizard.imageDecs.toggle(name == "chooseLayout");
      $("#title").text(wizard.strings.steps[name].title);
      $("#desc").html(wizard.strings.steps[name].desc);
      wizard.imageSelector.wijcombobox("option", "text", wizard.strings.loadingImage);
      wizard.startProgress();
      // get screenshots for the appropriate wizard step
      if (wizard.hasExternal(name)) {
        window.external[name]();
      }
      // adjust sizes of the widgets to the step page
      $(window).trigger("resize");
    },

    // show the wizard step with a theme preview screenshot
    // called from external code
    gotScreenshots: function (data) {
      wizard.progress.hide();
      $(document.body).css("cursor", "default");
      var obj = JSON.parse(data[0]);
      if (wizard.step == "themeBranding") {
        wizard.showThemeBranding(obj.Screenshots[0]);
        return;
      } else if (wizard.step == "targetBranding") {
        wizard.showTargetBranding(obj.Screenshots[0]);
        return;
      } else if (wizard.step == "themeNaming") {
        wizard.showThemeNaming(obj.Screenshots[0]);
        return;
      }
      // fill screenshots for the carousel 
      wizard.carousel.empty().fadeIn();
	    wizard.carousel.append('<div class="left carousel-control"/><div class="carousel-inner"/><div class="right carousel-control"/>');
      var items = [];
      $.each(obj.Screenshots, function (_, item) {
        $("<img/>").appendTo(wizard.carousel.find('.carousel-inner'))
            .attr("src", item.Path)
            .attr("width", "500px") // reduce screenshot width          
            .attr("title", item.Name)
            .data("value", item.Value);
        // prepare screenshot info for the combobox
        items.push({ label: item.Name, value: item.Value });
      });
      // create carousel widget
      wizard.carouselWidget = wizard.carousel.waterwheelCarousel({
        separation: 250, // how far to shift each screenshots from each other
        flankingItems: 2, // how many screenshots at each side of the selected screenshot
        keyboardNav: true, // allow to use keyboard arrow keys to scroll the carousel
        startingItem: obj.StartIndex + 1, // selected screenshot index
        containerWidth: "100%", // center carousel
        // select initial screenshot in the carousel and the combobox
        loaded: function () {
          wizard.imagesLoaded(items, obj.StartIndex);
        },
        // show carousel scrolling progress in the combobox 
        movingToCenter: function () {
          wizard.imageDecs.text(wizard.strings.loadingImage);
          wizard.imageSelector.wijcombobox("option", "text", wizard.strings.loadingImage);
        },
        // show selected screenshot info in the combobox
        movedToCenter: function ($item) {
          wizard.imageSelected($item.data("value"), $item.attr('title'), items);
        }
      });
    },

    // checks theme name and finishes the wizard
    // called from external code
    finish: function () {
      var err;
      var errElem = $("#themeNameError");
      // stop listening user activity
      clearInterval(wizard.intervals.themeName);
      // hide previous error message 
      errElem.fadeOut();
      // verify the name theme name
      if (!wizard.themeName.wijinputmask("option", "disabled")) {
        if (wizard.hasExternal("verifyThemeName")) {
          err = window.external.verifyThemeName(wizard.themeName.val());
          errElem.text(err).fadeIn();
        }
      }
      // try to finish external code
      if (!err) {
        if (wizard.hasExternal("finish")) {
          err = window.external.finish();
        }
        // if finish fails show error message
        errElem.text(err).fadeIn();
        if (err) {
          errElem.focus();
        }
      }
      // if error then clear it when user changes the theme name
      if (err) {
        var oldname = wizard.themeName.val();
        wizard.intervals.themeName = setInterval(function () {
          if (oldname != wizard.themeName.val()) {
            clearInterval(wizard.intervals.themeName);
            errElem.fadeOut();
          }
        }, 500);
      }
    },

    /* wizard steps */

    // initializes "theme branding" wizard step page
    themeBranding: function () {
      var disable = false;
      // for predefined themes they branding cannot be changed
      if (wizard.hasExternal("disableBranding")) {
        disable = window.external.disableBranding();
      }
      $("#readonlyTheme").toggle(disable);
      $("#brandingWarning").toggle(window.external.warnBranding());
      $("#chooseFavIcon").button("option", "disabled", disable);
      $("#chooseLogo").button("option", "disabled", disable);
      $("#removeFavIcon").button("option", "disabled", disable);
      $("#removeLogo").button("option", "disabled", disable);
      $("#showTitle").wijcheckbox("option", "disabled", disable);
      $("#fitTextWidth").wijcheckbox("option", "disabled", disable);
      wizard.headerHeight.wijinputnumber("option", "disabled", disable);
      wizard.maxTextWidth.wijinputnumber("option", "disabled", disable);
    },

    // shows the "theme branding" wizard step page
    showThemeBranding: function (screenshot) {
      wizard.showPreview(screenshot);
      wizard.settings = JSON.parse(screenshot.Value);
      var logoImage = wizard.settings.pageHeader.logoImage || wizard.settings.sidePanel.header.logoImage;
      $("#favIcon").attr("src", wizard.settings.general.favicon + '?r=' + (new Date()).getTime()).toggle(wizard.settings.general.favicon ? true : false);
      $("#logo").attr("src", logoImage + '?r=' + (new Date()).getTime()).toggle(logoImage ? true : false);
      $("#showTitle").wijcheckbox("option", "checked", wizard.settings.pageHeader.showText);
      $("#fitTextWidth").wijcheckbox("option", "checked", wizard.settings.topic.textWidth ? false : true);
      $("#removeFavIcon").toggle(wizard.settings.general.favicon ? true : false);
      $("#removeLogo").toggle(logoImage ? true : false);
      // hack: need to store initial value to avoid unwanted screenshot preview refreshing when value will be assigned
      wizard.headerHeight.data("initvalue", wizard.settings.pageHeader.height);
      wizard.maxTextWidth.data("initvalue", wizard.settings.topic.textWidth || wizard.defaultMaxTextWidth);
      wizard.headerHeight.wijinputnumber("option", "value", wizard.settings.pageHeader.height);
      wizard.maxTextWidth.wijinputnumber("option", "value", wizard.settings.topic.textWidth || wizard.defaultMaxTextWidth);
      // disable max text width if text fits to the window width
      wizard.maxTextWidth.wijinputnumber("option", "disabled", wizard.settings.topic.textWidth ? false : true);
      $("#themeBrandingSpliiter").show();
      wizard.stepLoaded();
    },

    // performs actions after step has been loaded 
    stepLoaded: function () {
      $(window).trigger('resize');
      // notify externals the wizard step is loaded
      if (wizard.hasExternal("stepLoaded")) {
        window.external.stepLoaded();
      }
      document.body.focus();
    },

    // initializes "target branding" wizard step page
    targetBranding: function () {
      var items = [], data;
      // load the theme available languages to the combobox
      if (wizard.hasExternal("getLanguages")) {
        data = JSON.parse(window.external.getLanguages());
        $.each(data.Screenshots, function (_, item) {
          items.push({ label: item.Name, value: item.Value });
        });
      }
      wizard.languageSelector.wijcombobox("option", "data", items);
      // select initial language in the combobox
      wizard.languageSelector.wijcombobox("option", "selectedValue", items.length > 0 ? items[data.StartIndex].value : "");
      wizard.languageSelector.wijcombobox("option", "text", items.length > 0 ? items[data.StartIndex].label : "");
      // initialize widget with the current value to avoid updating settings until the value is not set
      wizard.targetTitle.data("initvalue", wizard.targetTitle.val());
      // listen user activity on the "target title" textbox
      wizard.intervals.targetTitle = setInterval(function () {
        var text = wizard.targetTitle.val();
        // ignore changes if the value changed not by the user
        // we do some trick: assigning "initvalue" before changing the widget value
        if (wizard.targetTitle.data("initvalue") != text) {
          wizard.targetTitle.data("initvalue", text);
          // apply value immediately to the theme settings
          applyValue(false);
          // refresh the theme preview screenshot in 1 sec to avoid frequent refreshes 
          setTimeout(function () {
            applyValue(true);
          }, 1000);
        }

        // applay value to the theme settings
        function applyValue(refreshPreview) {
          if (wizard.hasExternal("updateTargetBranding")) {
            // don't do it if value is changed we are moved on another wizard step
            if (text != wizard.targetTitle.val() || !wizard.targetTitle.is(":visible")) return;
            refreshPreview && wizard.previewImages.fadeOut();
            wizard.settings.strings.pageHeaderText = text;
            var setter = { strings: { pageHeaderText: text } };
            window.external.updateTargetBranding(JSON.stringify(setter), refreshPreview);
          }
        }
      }, 500);
    },

    // shows the "target branding" wizard step page
    showTargetBranding: function (screenshot) {
      wizard.showPreview(screenshot);
      wizard.settings = JSON.parse(screenshot.Value);
      // hack: need to store initial value to avoid unwanted screenshot preview refreshing when value will be assigned
      wizard.targetTitle.data("initvalue", wizard.settings.strings.pageHeaderText);
      wizard.targetTitle.wijinputmask("option", "text", wizard.settings.strings.pageHeaderText);
      $("#section508").wijcheckbox("option", "checked", wizard.settings.general.accessibilityMode == "Section 508");
      $("#targetBrandingSplitter").show();
      $(window).trigger('resize');
      // adjust width of widgets to the panel size
      wizard.targetTitle.width("89%").parents('.wijmo-wijinput').width('89%');
      wizard.adjustCombobox(wizard.languageSelector, "#targetBrandingProperties");
      wizard.stepLoaded();
    },

    // initializes "target branding" wizard step page
    themeNaming: function () {
      var isnew = true;
      if (wizard.hasExternal("selectedThemeIsNew")) {
        isnew = window.external.selectedThemeIsNew();
      }
      // hide previous error text
      $("#themeNameError").hide();
      // disable "theme name" if it is not a new theme
      $("#themeNameDesc").toggle(isnew);
      wizard.themeName.wijinputmask("option", "disabled", !isnew);
      $(window).trigger('resize');
    },

    // shows the "theme naming" wizard step page
    showThemeNaming: function (screenshot) {
      wizard.showPreview(screenshot);
      // disable theme name widget if an existing theme selected
      var disabled = wizard.themeName.wijinputmask("option", "disabled");
      // empty theme name widget if a new theme selected
      wizard.themeName.wijinputmask("option", "text", disabled ? screenshot.Name : "");
      wizard.stepLoaded();
    },

    /* utils */

    // check whether the external function exists
    hasExternal: function (name) {
      return window.external && name in window.external;
    },

    // shows the theme preview screenshot
    showPreview: function (screenshot) {
      $("[data-c1role='previewImageHeader']").text(screenshot.Name);
      wizard.previewImages.attr("src", screenshot.Path + '?r=' + (new Date()).getTime() /* avoid caching */)
        .attr("title", screenshot.Name).fadeIn();
    },

    // select initial screenshot in the carousel and the combobox
    imagesLoaded: function (items, startIndex) {
      wizard.imageSelector.wijcombobox("option", "disabled", false);
      wizard.imageSelector.wijcombobox("option", "data", items);
      var value = items.length > 0 ? items[startIndex].value : "";
      var text = items.length > 0 ? items[startIndex].label : "";
      // notify externals the selection is changed
      wizard.imageSelected(value, text, items);
      wizard.stepLoaded();
    },

    // select the screenshot in the carousel and the combobox
    imageSelected: function (value, text, items) {
      var index = -1;
      var valueDesc = wizard.strings.layouts[value];
      // get screenshot index
      $.each(items, function (i, item) {
        if (item.value == value) {
          index = i;
          return false;
        }
      });
      wizard.imageSelector.wijcombobox("option", "selectedIndex", index);
      // wijmo bug: "selectedIndex" is not enough to show item in text area
      wizard.imageSelector.wijcombobox("option", "selectedValue", value);
      // wijmo bug: "selectedValue" is not enough to show item in text area
      wizard.imageSelector.wijcombobox("option", "text", text);
      wizard.imageSelector.wijcombobox("close");
      wizard.imageDecs.text(valueDesc || wizard.strings.layouts.Unknown);
      // notify externals the selection is changed
      if (wizard.hasExternal("selectedScreenshotChanged")) {
        window.external.selectedScreenshotChanged(value); 
      }
    },

    // shows start of getting screenshots progress
    // called from external code
    startProgress: function () {
      wizard.previewImages.hide();
      wizard.progressLabel.text("0%");
      wizard.progress.show();
    },

    // updates the progress of getting screenshots
    // called from external code
    screenshotProgress: function (data) {
      wizard.progressLabel.text((parseInt(data[0] / data[1] * 100, 10) + '%'));
    },

    // updates the progress text
    // called from external code
    progressText: function (text) {
      wizard.progressLabel.text(text);
    },

    // fades out the preview screenshot
    // called from external code
    fadeOutPreview: function () {
      wizard.previewImages.fadeOut();
    },

    // adjusts comboox width to the panel siza
    adjustCombobox: function (elem, parent) {
      var box = $(elem).parents(".wijmo-wijcombobox");
      box.width($(parent).innerWidth() - box.offset().left - 20);
      elem.width(box.find(".wijmo-wijcombobox-wrapper").innerWidth() - box.find(".wijmo-wijcombobox-trigger").outerWidth());
    },
    
    // adjusts preview image to the page size
    adjustPreviewImage: function (elem) {
      if ($(elem).is(":visible")) {
        $(elem).height($(window).innerHeight() - $(elem).find(".preview-image").offset().top - 10);
      }
    },

    // workaround for wijinputnumber widgets
    // don't allow clear the value when delete key is pressed
    // called from the external code
    processKey: function (code) {
      var inp = $(document.activeElement);
      if (!inp.hasClass('wijmo-wijinput-numeric') && inp.find('.wijmo-wijinput-numeric').length) {
        inp = inp.find('.wijmo-wijinput-numeric');
      }
      if (inp.hasClass('wijmo-wijinput-numeric') || code != 46) {
        var e = $.Event('keydown', { keyCode: code });
        inp.trigger(e);
        e = $.Event('keyup', { keyCode: code });
        inp.trigger(e);
      } else if (inp.hasClass('wijmo-wijcombobox-input') && code == 46) {
        // prevent deletion in wijmo comboboxes: do nothing
      } else {
        document.execCommand('Delete');
      }
    },

    // returns false if a dropdown is open
    nextStepAllowed: function () {
      return !wizard.dropdownOpened;
    },

    // close all dropdowns
    closeDropdown: function () {
      wizard.imageSelector.wijcombobox("close", null, true);
      wizard.languageSelector.wijcombobox("close", null, true);
    },

    // updates numeric value in the theme settings
    // elem: jquery element where value is changed
    // value: new numeric value
    // wizard timeout id
    // applyValue: function to update value in the theme settings
    updateNumericValue: function (elem, value, timeoutId, applyValue) {
      // cancel the previous waiting of refreshing the theme preview screenshot
      clearTimeout(wizard.timeOuts[timeoutId]);
      // ignore the event when it raises due initialization
      if (!elem.data("inited")) {
        elem.data("inited", true);
        // ignore the event if the value changed not by the user
        // we do some trick: assigning "initvalue" before changing the widget value
      } else if (elem.data("initvalue") != value) {
        elem.data("initvalue", value);
        // wijmo bug value can be greater than maxValue
        // fix:set value to allowed maximim and update widget
        var max = elem.wijinputnumber("option", "maxValue");
        if (value > max) {
          elem.wijinputnumber("option", "value", max);
          return;
        }
        // wijmo bug value can be less than minValue
        // fix:set value to allowed minimun and update widget 
        var min = elem.wijinputnumber("option", "minValue");
        if (value < min) {
          elem.wijinputnumber("option", "value", min);
          return;
        }
        // apply value immediately to the theme settings
        applyValue(false);
        // refresh the theme preview screenshot in 1 sec to avoid frequent refreshes 
        wizard.timeOuts[timeoutId] = setTimeout(function () {
          applyValue(true);
        }, 1000);
      }
    },

    // updates bool value in the theme settings
    // elem: jquery element where value is changed
    // wizard timeout id
    // applyValue: function to update value in the theme settings
    updateBoolValue: function (elem, timeoutId, applyValue) {
      // cancel the previous waiting of refreshing the theme preview screenshot
      clearTimeout(wizard.timeOuts[timeoutId]);
      // wijmo bug workaround: ignore click if the checkbox is disabled
      if (elem.wijcheckbox("option", "disabled")) {
        return;
      }
      // apply value immediately to the theme settings
      applyValue(false);
      // refresh the theme preview screenshot in 1 sec to avoid frequent refreshes 
      wizard.timeOuts[timeoutId] = setTimeout(function () {
        applyValue(true);
      }, 500);
    }
  };  
})(jQuery);
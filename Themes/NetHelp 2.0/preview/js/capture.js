/* 
* Provides methods to make screenshots from the NetHelp
*/

// Notifies external handlers that the NetHelp is ready for make a screenshot
window.nethelp.shell.bind('topicready', function (e, p) {
  if (window.external && 'onTopicReady' in window.external) {
    setTimeout(function () {
      // need to put the method into the effect queue to be sure all content is rendered correctly
      $('#c1content').queue(function () {
        window.external.onTopicReady();
        $(this).dequeue();
      });
    }, 1);
  }
});

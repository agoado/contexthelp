/* 
* Provides methods to pass to the NetHelp external settings
*/

// Gets the external NetHelp settings 
function getNethelpExternalSettings() {
  if (window.external && 'onGetNethelpExternalSettings' in window.external) {
    return {
      type: "json",
      settings: window.external.onGetNethelpExternalSettings || '{}'
    };
  }
  return null;
};

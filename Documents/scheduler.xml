<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link href="../CSSFiles/additional.css" rel="stylesheet" type="text/css" />
    <title>User Guide</title>
    <meta charset="utf-8" />
    <meta name="spellchecker-language" content="en-US" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="../CSSFiles/C1H_Internal.css" />
  </head>
  <body>
    <h1>Scheduler</h1>
    <p class="Warning"><strong><img alt="" src="../Media/warning.png" />&nbsp;Note</strong><br />The <strong>Scheduler Service</strong> is an optional feature and may not be enabled in all systems. Check with the system administrator for additional information.</p>
    <p>The <strong>Scheduler Service</strong> executes reports on periodic intervals. This can be useful for reports which are based on periods of time, such as weekly sales reports or quarterly earnings reports.</p>
    <p class="MidTopic">Making a Schedule</p>
    <p>Schedules are created and edited with the Schedule Report Wizard, which is a tool designed to streamline schedule creation.</p>
    <p>To schedule a report:</p>
    <ol>
      <li class="list_123">
        <p>Click the <strong>Menu </strong><img alt="" src="../Media/screen.report_tree_menu.png" />&nbsp;icon and select <img src="../Media/ScheduleAddSmall.png" />&nbsp;<strong>Schedule Report</strong>.</p>
      </li>
      <li class="list_123">
        <p>Enter a <strong>Name </strong>for the schedule.</p>
      </li>
      <li class="list_123">
        <p>Choose which file type to save the report as.</p>
      </li>
      <li class="list_123">
        <p><em>Optional</em>: For PDF or Excel, enter a <strong>Password </strong>to secure the report.</p>
      </li>
      <li class="list_123">
        <p>Select a date and time for the schedule, and choose how often it should run:</p>
        <ol>
          <li class="list_abc">
            <p>In the <strong>Schedule Time </strong>field, enter a time for when the schedule should run.</p>
          </li>
          <li class="list_abc">
            <p><em>Optional</em>: To run the schedule as soon as it is created, select the <strong>Execute Immediately </strong>check box. This schedule cannot be given a recurrence pattern. Go to step 6.</p>
          </li>
          <li class="list_abc">
            <p><em>Optional</em>: To run the schedule multiple times per day, select the <strong>Repeat Every </strong>check box and enter a time for how often it should repeat.</p>
          </li>
          <li class="list_abc">
            <p><em>Optional</em>: To run the schedule on more than one day, choose a <strong>Recurrence Pattern </strong>from the available options:</p>
            <p><span class="Option">Daily</span></p>
            <p class="Indent">The schedule will repeat every day, every weekday, or every number of days. Enter a number of days for how often the schedule should repeat, or select <strong>Every weekday </strong>to run it every Mon, Tues, Wed, Thurs, and Fri.</p>
            <p><span class="Option">Weekly</span></p>
            <p class="Indent">The schedule will repeat every week, or every number of weeks, on one or more days. Enter a number of weeks for how often the schedule should repeat, and select one or more days when it should run.</p>
            <p><span class="Option">Monthly</span></p>
            <p class="Indent">The schedule will repeat every month, or every number of months, on a certain day. Enter a day for when the schedule should run, and a number of months for how often it should repeat.</p>
            <p><span class="Option">Yearly</span></p>
            <p class="Indent">The schedule will repeat every year on a certain day. Enter a day for when the schedule should run.</p>
          </li>
          <li class="list_abc">
            <p><em>Optional</em>: If you have entered a recurrence pattern, enter a date for when the schedule should <strong>Start</strong>. </p>
          </li>
        </ol>
      </li>
      <li class="list_123">
        <p><em>Optional:</em> To specify values for prompting parameters on the report, click the <strong>Parameters</strong> tab.<br /><em>Optional</em>: To add filters to the report, click the <strong>Filters </strong>tab. If the report already has filters you can set their values here. See <c1hjump d2hml="topic=Report Designer\: Standard Filters">Filters</c1hjump> for instructions.</p>
      </li>
      <li class="list_123">
        <p><em>Optional</em>: To email this schedule to addresses from a database, see <c1hjump d2hml="topic=Emailing Personalized Reports (Batch Scheduling);document=Documents\internal_help.xml">Emailing personalized reports</c1hjump>.</p>
      </li>
      <li class="list_123">
        <p>Click the <strong>Recipients </strong>tab. To send the report to a list of email addresses, select the <strong>Email Reports </strong>check box and fill out the following fields:</p>
        <ol>
          <li class="list_abc">
            <p>Enter the recipients' email addresses in the <strong>To</strong>, <strong>Cc</strong>, and <strong>Bcc </strong>fields.</p>
          </li>
          <li class="list_abc">
            <p>Optional: You may be able to enter a reply-to address in the <strong>Reply To</strong> field. This will be the address that receives any replies to the emailed report.</p>
          </li>
          <li class="list_abc">
            <p>Optional: Enter an email subject in the <strong>Subject</strong> field.</p>
          </li>
          <li class="list_abc">
            <p>Optional: Enter an email message in the large text field.</p>
          </li>
        </ol>
        <p>If you do not want to email the report, clear the <strong>Email Reports</strong> check box. The report will be saved to disk instead. You may have to ask the&nbsp;system administrator for the file location.</p>
      </li>
      <li class="list_123">
To execute a test run before finalizing the schedule, click the <strong>Test E-Mail</strong> button. 
        <ol>
          <li class="list_123">The <strong>Test Recipients</strong> textbox will be pre-filled with the e-mail addresses from the <strong>To:</strong> field. Add, edit or delete e-mail addresses to send the test to in this textbox. </li>
          <li class="list_123">
Click <strong>Send</strong> to send the schedule’s email now. This is generally used to check the email’s content and formatting. 
            <p class="text-align:center"><img alt="DRTZY2kV3A.png" src="../Media/scheduler-wizard-1.png" /></p>
            <p class="Caption">Test message successful dialog</p>
          </li>
        </ol>
      </li>
      <li class="list_123">
        <p><em>Optional</em>: If this is a batch schedule, an <strong>Attach Report Output to Email </strong>check box is available. Clear this check box if you want to use this report to send alerts, and you do not want to email the report itself.</p>
      </li>
      <li class="list_123">
        <p>Click <strong>Finish</strong> to save the schedule.</p>
      </li>
    </ol>
    <p>Schedules can be edited after they are created. See <c1hjump d2hml="tag=Viewing_and_Editing_Schedules">Viewing and Editing Schedules</c1hjump> for more information.</p>
    <p class="MidTopic">Viewing and Editing Schedules</p>
    <p>Click the <img src="../Media/MainLeftPaneScheduleManager.png" /><strong>Schedule Manager&nbsp;</strong>icon to open the Scheduler Manager. Depending on system configuration, you may be able to see reports scheduled by other users. Check with the system administrator for more information.</p>
    <p style="text-align: center;"><img alt="" src="../Media/scheduler-manager-1.png" /></p>
    <p>Each schedule is on a row with its most recent and forthcoming run times, and a status indicating its condition:</p>
    <ul>
      <li class="list_bullets"><strong>Ready</strong> — the schedule will run on its next run time</li>
      <li class="list_bullets"><strong>Running</strong> — the schedule is currently running.</li>
      <li class="list_bullets"><strong>Transmitting</strong> — the scheduled event has finished running, and the report is being sent to the user.</li>
      <li class="list_bullets"><strong>Completed</strong> — the schedule has completed its final event, and will not run again. It will be removed from the list when the cache is flushed.</li>
      <li class="list_bullets"><strong>Deleted</strong> — the schedule has been deleted, and will be removed from the list when the cache is flushed.</li>
      <li class="list_bullets"><strong>Abended</strong> — the last run failed due to an error. The schedule will not run again.</li>
      <li class="list_bullets"><strong>User Aborted</strong> — the schedule is running, but it was requested to be canceled. It will be marked as Deleted.</li>
    </ul>
    <p>Scheduled events which have passed are copied to an archive row, so that there is a log of previous run times. Such events are labeled <code>(archive)</code>. Deleting these rows will not affect the main schedule.</p>
    <p>Schedules which are associated with a&nbsp;report using execution caching&nbsp;are labeled <code>(cache)</code>.&nbsp;See <c1hjump d2hml="tag=Execution_Caching">Execution Caching</c1hjump> for more information.</p>
    <p>Schedules which were e-mailed with the E-Mail Reports features are labeled <code>(email)</code>. See Navigating the Application for more information.</p>
    <p>Test runs of schedules sent with the Test Email button are labeled <code>(test)</code>.</p>
    <p class="Aside"><strong>Managing schedules</strong></p>
    <p>To edit an existing schedule click the <strong>Edit <img src="../Media/Edit.png" /> </strong>icon at the end of the row. Changes to the recurrence pattern will affect all forthcoming run times. You can reuse or reset deleted or completed schedules by editing them. This will set their status to Ready.</p>
    <p>To delete a schedule click the <strong>Delete <img src="../Media/DeleteItem.png" /> </strong>icon at the end of the row. This sets its status to Deleted. If the schedule is already marked as Deleted, this removes the schedule from the list.</p>
    <p>Completed and canceled schedules and archives may be periodically removed from the list. Click <img src="../Media/Flush.png" />&nbsp;<strong>Flush </strong>to remove them immediately</p>
    <p class="MidTopic">Emailing Personalized Reports (Batch Scheduling)</p>
    <p>Batch scheduling allows you to send personalized versions of reports to a list of email addresses. The addresses must be defined in a data object, the unique key of which is used as a blanket filter for the report. Each recipient receives a customized version, filtered by their unique key.</p>
    <p>For example, if you wanted to send out a customized sales review, or a pre-filled tax form, or even a set of customized forms for each employee, batch scheduling allows you use one report for every person. This also has the benefit of only needing to run the report once. So you are not making repeated calls to the database, which could be slow and system-intensive.</p>
    <p class="Warning"><strong><img alt="" src="../Media/warning.png" />&nbsp;WARNING</strong><br />Batch emailing should only be used as instructed by the system administrator. If you are unsure about how to proceed, do not create a batch schedule. Ask the administrator for assistance.</p>
    <p>Batch scheduling requires you to have access to a data&nbsp;object with a column of email addresses, that must be added in the report <c1hjump d2hml="topic=Report Designer\: Data Objects">Data Objects</c1hjump> menu. The&nbsp;data object&nbsp;does not have to be in the body of the report.</p>
    <p>To make a batch schedule:</p>
    <ol>
      <li class="list_123">
        <p>In the Schedule Report Wizard, click the <strong>Batch </strong>tab.</p>
      </li>
      <li class="list_123">
        <p>Select the <strong>Run as Batch Report </strong>check box.</p>
      </li>
      <li class="list_123">
        <p>Optional: In the <strong>To</strong> and <strong>Cc</strong> fields, enter addresses to send a summary email for each completed execution.</p>
      </li>
      <li class="list_123">
        <p>From the <strong>Batch Email Field </strong>list, select the data field containing the email addresses for this schedule.</p>
      </li>
      <li class="list_123">
        <p>Optional: Click the <strong>Recipients</strong> tab to enter a subject and message for the batch email.</p>
        <p>You can reference data values from the email address data row in the message body. Type the parameter <span class="Code">@batch_<em>fieldname</em>@</span>, where <em>fieldname </em>is the name of a data field in the address data object (in <strong><em>lowercase</em></strong> text). The value of the field replaces the parameter text in the message output.</p>
        <p class="Tip"><img alt="" src="../Media/lightbulb.png" />&nbsp;<strong>Tip</strong><br />The <strong>To</strong>, <strong>Cc</strong>, and <strong>Bcc</strong> fields are unavailable when using Batch.</p>
      </li>
    </ol>
  </body>
</html>